import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FilterRestaurantModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-filter-restaurant-modal',
  templateUrl: 'filter-restaurant-modal.html',
})
export class FilterRestaurantModalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FilterRestaurantModalPage');
  }

  closeModal(){
    this.navCtrl.pop();
  }

}
