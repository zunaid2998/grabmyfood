import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, ActionSheetController } from 'ionic-angular';
import { RestaurantServiceProvider } from '../../providers/restaurant-service';
import { ItemOptionsModalPage } from '../item-options-modal/item-options-modal';
import { Restaurant, Menu, Category } from '../../core/model';

@Component({
  selector: 'page-restaurant',
  templateUrl: 'restaurant.html',
})
export class RestaurantPage {
  restaurant: Restaurant;
  viewCart: boolean;
  dataLoaded: boolean;
  ngOnInit(){}

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private modalCtrl: ModalController,
              private restaurantProvider: RestaurantServiceProvider,
              private loadingCtrl: LoadingController,
              private actionSheetCtrl: ActionSheetController) {
  }

  ionViewDidEnter(){
    let restaurantId = this.navParams.get('restaurantId');
    this.loadRestaurant(restaurantId);
  }

  loadRestaurant(restaurantId: number){
    const loadCtrl = this.loadingCtrl.create({
      content: "Fetching Restaurant"
    });
    loadCtrl.present();
    
    this.restaurantProvider.getRestaurantDetail(restaurantId)
      .subscribe((data: any)=>{
        this.dataLoaded = true;
        this.restaurant = data.restaurant;
        loadCtrl.dismiss();
      }, (error)=> {loadCtrl.dismiss()});
  }

  openItemOptionsModal(menu: Menu){
    let itemOptionsModal = this.modalCtrl.create(ItemOptionsModalPage, {menu: menu, restaurant: this.restaurant});
    itemOptionsModal.present();
  }
  
  scrollToCategory(category: Category){
    let selectedCategory = document.getElementById('category-' + category.id);
    selectedCategory.scrollIntoView(true)
  }

  openFilterCategoryPanel(){
    let buttons: any[] = [];
    this.restaurant.categories.forEach(cat=> {
      buttons.push({
        text: cat.name,
        handler: () => { this.scrollToCategory(cat)}
      })
    })
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Pick your category',
      buttons: buttons,
      cssClass: 'generic-action-sheet'
    });
    actionSheet.present();
  }
}
