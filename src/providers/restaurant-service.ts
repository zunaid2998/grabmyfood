import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Restaurant } from '../core/model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RestaurantServiceProvider {

  apiUrl: string = "http://grabmyfood.ahmedzunaid.com/api/restaurant";

  constructor(public http: HttpClient) {}

  getRestaurants(): Observable<Restaurant[]> {
    return this.http.get<Restaurant[]>(`${this.apiUrl}/all`);
  }

  getRestaurantDetail(id: number): Observable<Restaurant>{
    return this.http.get<Restaurant>(`${this.apiUrl}/${id}/menu`)
  }

}
