import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { CardIO, CardIOResponse, CardIOOptions } from '@ionic-native/card-io';
import { Stripe } from '@ionic-native/stripe';

/**
 * Generated class for the AddPaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-payment',
  templateUrl: 'add-payment.html',
})
export class AddPaymentPage {
  cardType: string;
  redactedCardNumber: string;
  cardNumber: string;
  expiryMonth: number;
  expiryYear: number;
  cvv: string;
  cardholderName: string;

  get expiryDate() : string { return this.expiryMonth+ ' / ' + this.expiryYear }

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private cardIO: CardIO, 
              private alertCtrl: AlertController,
              private stripe: Stripe) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPaymentPage');
  }

  closeModal(){
    this.navCtrl.pop();
  }

  scanCard(){
    this.cardIO.canScan()
      .then( (res: boolean) => {
        if(res){
          let options : CardIOOptions = {
            requireExpiry: true,
            requireCVV: true,
            requireCardholderName: true,
            requirePostalCode: false
          }
          this.cardIO.scan(options)
            .then( (response: CardIOResponse) => {
              this.cardNumber = response.cardNumber;
              this.cardholderName = response.cardholderName;
              this.expiryMonth = response.expiryMonth;
              this.expiryYear = response.expiryYear;
              this.cvv = response.cvv;
              this.alertCtrl.create({
                message: this.expiryDate
              }).present();
            })
            .catch(error => { console.log(error)})
        }
      })
  }

  SaveCard(){
    this.stripe.setPublishableKey("pk_test_7rfXSmno7Y9e1KVtepzoILdg");
    let card = {
      number: this.cardNumber,
      expMonth: this.expiryMonth,
      expYear: this.expiryYear,
      cvc: this.cvv
     };
     this.alertCtrl.create({
        message: this.cardNumber
      }).present();
    this.stripe.validateCardNumber(this.cardNumber).then( data => {
      this.alertCtrl.create({
        message: "Card number is valid"
      }).present();
    }).catch(error => {
      this.alertCtrl.create({
        message: "Card number is invalid"
      }).present();
    });

    //this.navCtrl.pop();
  }

}
