import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Restaurant } from '../../core/model';
import { RestaurantServiceProvider } from '../../providers/restaurant-service';
import { RestaurantPage } from '../restaurant/restaurant';

@Component({
  selector: 'page-search-result',
  templateUrl: 'search-result.html',
})
export class SearchResultPage {
  searchText: string;
  get placeholderText() : string { return "Search any cuisine or dishes" }
  restaurants: Restaurant[];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private restaurantService: RestaurantServiceProvider) {
    this.searchText = this.navParams.get('searchText');
    console.log(this.searchText);
    this.restaurantService.getRestaurants()
     .subscribe((restaurants: Restaurant[])=> {
        this.restaurants = restaurants;
     })
  }

  openRestaurant(id: number){
    this.navCtrl.push(RestaurantPage, {
      restaurantId: id
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchResultPage');
  }

  onInput(event){console.log(event)}
  onCancel(event){console.log(event)}

}
