import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

declare const Pusher: any;
@Injectable()
export class PusherServiceProvider {
  channel;
  pusherkey: string = "f030a388c59d43334fe8";
  constructor(public http: HttpClient) {
    var pusher = new Pusher(this.pusherkey, {
      cluster: "us2",
      encrypted: false
    });
    this.channel = pusher.subscribe("my-channel");
  }
  public init() {
    return this.channel;
  }
}
