import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SearchServiceProvider } from '../../providers/search-service';
import { SearchResultPage } from '../search-result/search-result';

@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  searchText: string;
  get placeholderText() : string { return "Search any cuisine or dishes" }
  cuisines: any[] = []

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public searchService: SearchServiceProvider) {
    this.cuisines = this.searchService.getDefaultCuisines();
  }

  ionViewDidLoad() {
    
  }

  onSelectCuisine(cuisineName: String){
    this.navCtrl.push(SearchResultPage, {
      searchText: cuisineName
    },{
      animate: false
    });
  }

  onInput(event){console.log(event)}
  onCancel(event){console.log(event)}

}
