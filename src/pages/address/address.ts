import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-address',
  templateUrl: 'address.html',
})
export class AddressPage {
  googleAutocompleteService: any;
  autocompleteInput: any;
  autocompleteItems: any;
  zone: any;
  google: any
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    
  }

  addressChanged(data){
    console.log(data);
  }

  ionViewDidEnter(){
    
  }

  updateSearchResults(){
    
  }

  selectSearchResult(data){
    console.log(data);
  }

  closeModal(){
    this.navCtrl.pop();
  }

}
