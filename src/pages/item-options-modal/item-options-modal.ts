import { Component } from '@angular/core';
import { NavController, NavParams, Events, ModalController } from 'ionic-angular';
import { AddInstructionPage } from '../add-instruction/add-instruction';
import { Menu, Cart, Restaurant } from '../../core/model';

@Component({
  selector: 'page-item-options-modal',
  templateUrl: 'item-options-modal.html',
})
export class ItemOptionsModalPage {
  menu: Menu;
  restaurant: Restaurant;
  menuPriceOnCart: number;
  cart: Cart;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public addToCartEvent: Events,
              public modalCtrl: ModalController) {
    this.menu = this.navParams.get('menu');
    this.restaurant = this.navParams.get('restaurant');
    if(this.menu){
      this.menu.quantity = 1;
      this.menuPriceOnCart = this.menu.price;
    }
  }

  onClickPlusIcon(){
    this.menu.quantity = this.menu.quantity + 1;
    this.updateMenuPrice()
  }

  updateMenuPrice(){
    this.menuPriceOnCart = this.menu.quantity * this.menu.price;
  }

  onClickMinusIcon(){
    if(this.menu.quantity > 1) {
      this.menu.quantity = this.menu.quantity - 1;
      this.updateMenuPrice();
    }
  }

  onClickAddToCart() {
    if(!this.cart){
      this.cart = new Cart(this.restaurant);
    }
    this.cart.menus.push(this.menu);
    this.addToCartEvent.publish('itemAddedToCart', {cart: this.cart});
    this.closeModal();
  }

  closeModal(){
    this.navCtrl.pop();
  }

  onClickNotes(){
    this.modalCtrl.create(AddInstructionPage).present();
  }

}
