import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { PaymentDetailPage } from '../payment-detail/payment-detail';
import { AddPaymentPage } from '../add-payment/add-payment';

/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {}

  onClickAddPaymentCard(){
    let paymentPage = this.modalCtrl.create(AddPaymentPage);
    paymentPage.present();
  }

  onClickPaymentDetail(){
    this.navCtrl.push(PaymentDetailPage)
  }

}
