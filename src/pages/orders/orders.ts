import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PusherServiceProvider } from '../../providers/pusher-service';
import { Order } from '../../core/model';

@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class OrdersPage {
  orderType: string;
  event: string = "my-event";
  upcomingOrders: Order[] = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, private pusher: PusherServiceProvider) {
    this.orderType = 'upcomingOrders';
  }

  ionViewDidLoad() {
    const channel = this.pusher.init();
      channel.bind(this.event, data => {
        if(data) {console.log(data);}
    });
  }

  orderSegmentChanged($event){
    
    console.log(this.orderType);
  }

}
