import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import { Events, ModalController } from 'ionic-angular';
import { CartPage } from '../cart/cart';
import { ProfilePage } from '../profile/profile';
import { OrdersPage } from '../orders/orders';
import { SearchPage } from '../search/search';
import { Cart } from '../../core/model';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  viewCart: boolean;
  cart: Cart;
  totalCartPrice: number;

  tab1Root = HomePage;
  tab2Root = SearchPage;
  tab3Root = OrdersPage;
  tab4Root = ProfilePage;

  constructor(private addToCartEvent: Events,
              private modalCtrl: ModalController) {
    this.addToCartEvent.subscribe("itemAddedToCart", data => {
      this.totalCartPrice = 0;
      this.viewCart = true;
      this.cart = data.cart;
      this.cart.menus.forEach(menu => {
        this.totalCartPrice = this.totalCartPrice + (menu.quantity * menu.price);
      })
    })
  }

  onClickViewCart(){
    let cartPageModal = this.modalCtrl.create(CartPage, { cart: this.cart});
    cartPageModal.present();
  }
}
