import { DateTime } from "ionic-angular";

export class Owner {
    id: number
    email: string
    password: string
    name: string
    phone: string
    restaurants: Restaurant[]
}

export class Restaurant {
    id: number
    name: string
    address: string
    ownerId: number
    categories: Category[]
}

export class Category {
    id: number
    name: string
    restaurantId: number
    menus?: Menu[]
}

export class Menu {
    id: number
    name: string
    description?: string
    price: number
    imageUrl?: string
    categoryId: number
    quantity?: number
    menuChoices?: MenuChoice[]
}

export class MenuChoice {
    id: number
    name: string
    value: string
    required: boolean
    singleSelect: boolean
    maxSelection: number
    menuId: number
}

export class Cart {
    restaurant: Restaurant
    menus : Menu[]

    constructor(restaurant: Restaurant){
        this.menus = [];
        this.restaurant = restaurant;
    }
}

export class Driver {
    id: number
    name: string
    email: string
    password: string
    vehicleRegNo: string
    status: string
    joiningDate: DateTime
    resigningDate: DateTime
    phone: string
}

export class Customer {
    id: number
    email: string
    password: string
    name: string
    phone: string
}

export class Order {
    id: number
    date: DateTime
    restaurantId: number
    customerId: number
    menuId: number
    driverId: number
    status: string
}