import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RestaurantServiceProvider } from './restaurant-service';

@Injectable()
export class SearchServiceProvider {
  cuisines: any[] = [
    {
      type: 'cuisine',
      name: 'Asian',
      img: '../../assets/imgs/search/asian.gif',
      restaurantCount: Math.ceil(Math.random() * 100)
    },
    {
      type: 'cuisine',
      name: 'Chinese',
      img: '../../assets/imgs/search/chinese.gif',
      restaurantCount: Math.ceil(Math.random() * 100)
    },
    {
      type: 'cuisine',
      name: 'Korean',
      img: '../../assets/imgs/search/korean.gif',
      restaurantCount: Math.ceil(Math.random() * 100)
    },
    {
      type: 'cuisine',
      name: 'Vietnamese',
      img: '../../assets/imgs/search/vietnamese.gif',
      restaurantCount: Math.ceil(Math.random() * 100)
    },
    {
      type: 'cuisine',
      name: 'American',
      img: '../../assets/imgs/search/american.gif',
      restaurantCount: Math.ceil(Math.random() * 100)
    },
    {
      type: 'cuisine',
      name: 'Fast Food',
      img: '../../assets/imgs/search/fastfood.gif',
      restaurantCount: Math.ceil(Math.random() * 100)
    },
    {
      type: 'cuisine',
      name: 'Indian',
      img: '../../assets/imgs/search/indian.gif',
      restaurantCount: Math.ceil(Math.random() * 100)
    },
    {
      type: 'cuisine',
      name: 'Pakistani',
      img: '../../assets/imgs/search/pakistani.gif',
      restaurantCount: Math.ceil(Math.random() * 100)
    }
  ]

  constructor(public http: HttpClient, public restaurantProvider: RestaurantServiceProvider) {
    console.log('Hello SearchServiceProvider Provider');
  }

  getDefaultCuisines(){
    return this.cuisines;
  }

}
