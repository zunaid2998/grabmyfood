import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CardIO } from '@ionic-native/card-io';
import { Stripe } from '@ionic-native/stripe';

import { FilterRestaurantModalPage } from '../pages/filter-restaurant-modal/filter-restaurant-modal';
import { RestaurantPage } from '../pages/restaurant/restaurant';
import { RestaurantServiceProvider } from '../providers/restaurant-service';
import { HttpClientModule } from '@angular/common/http';
import { ItemOptionsModalPage } from '../pages/item-options-modal/item-options-modal';
import { CartPage } from '../pages/cart/cart';
import { ProfilePage } from '../pages/profile/profile';
import { PaymentPage } from '../pages/payment/payment';
import { PaymentDetailPage } from '../pages/payment-detail/payment-detail';
import { FavoritePage } from '../pages/favorite/favorite';
import { HelpPage } from '../pages/help/help';
import { PromotionsPage } from '../pages/promotions/promotions';
import { SettingsPage } from '../pages/settings/settings';
import { OrdersPage } from '../pages/orders/orders';
import { SearchPage } from '../pages/search/search';
import { AddInstructionPage } from '../pages/add-instruction/add-instruction';
import { PusherServiceProvider } from '../providers/pusher-service';
import { AddPaymentPage } from '../pages/add-payment/add-payment';
import { SearchServiceProvider } from '../providers/search-service';
import { SearchResultPage } from '../pages/search-result/search-result';
import { AddressPage } from '../pages/address/address';

import { GooglePlacesAutocompleteComponentModule } from 'ionic2-google-places-autocomplete';

@NgModule({
  declarations: [
    MyApp,
    SearchPage,
    OrdersPage,
    HomePage,
    TabsPage,
    FilterRestaurantModalPage,
    RestaurantPage,
    ItemOptionsModalPage,
    CartPage,
    ProfilePage,
    PaymentPage,
    PaymentDetailPage,
    FavoritePage,
    HelpPage,
    PromotionsPage,
    SettingsPage,
    AboutPage,
    AddInstructionPage,
    AddPaymentPage,
    SearchResultPage,
    AddressPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true
    }),
    GooglePlacesAutocompleteComponentModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SearchPage,
    OrdersPage,
    HomePage,
    TabsPage,
    FilterRestaurantModalPage,
    RestaurantPage,
    ItemOptionsModalPage,
    CartPage,
    ProfilePage,
    PaymentPage,
    PaymentDetailPage,
    FavoritePage,
    HelpPage,
    PromotionsPage,
    SettingsPage,
    AboutPage,
    AddInstructionPage,
    AddPaymentPage,
    SearchResultPage,
    AddressPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CardIO,
    Stripe,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestaurantServiceProvider,
    PusherServiceProvider,
    SearchServiceProvider
  ]
})
export class AppModule {}
