import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PaymentPage } from '../payment/payment';
import { FavoritePage } from '../favorite/favorite';
import { HelpPage } from '../help/help';
import { PromotionsPage } from '../promotions/promotions';
import { SettingsPage } from '../settings/settings';
import { AboutPage } from '../about/about';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
  }

  onClickPayment(){
    this.navCtrl.push(PaymentPage);
  }

  onClickFavorite(){
    this.navCtrl.push(FavoritePage);
  }

  onClickHelp(){
    this.navCtrl.push(HelpPage);
  }

  onClickPromotions(){
    this.navCtrl.push(PromotionsPage);
  }

  onClickSettings(){
    this.navCtrl.push(SettingsPage);
  }

  onClickAbout(){
    this.navCtrl.push(AboutPage);
  }

}
