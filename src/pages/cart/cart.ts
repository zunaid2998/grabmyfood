import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { Cart, Restaurant } from '../../core/model';
import { AddressPage } from '../address/address';

@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {
  cart: Cart;
  restaurant: Restaurant;
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
    this.cart = this.navParams.get('cart');
    this.restaurant = this.cart.restaurant;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
  }

  closeModal(){
    this.navCtrl.pop();
  }

  onClickCustomerAddress(){
    this.modalCtrl.create(AddressPage, {}).present();
  }

}
