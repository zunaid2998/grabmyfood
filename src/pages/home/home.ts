import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { FilterRestaurantModalPage } from '../filter-restaurant-modal/filter-restaurant-modal';
import { RestaurantPage } from '../restaurant/restaurant';
import { RestaurantServiceProvider } from '../../providers/restaurant-service';
import { Restaurant } from '../../core/model';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  restaurants: Restaurant[];
  constructor(public navCtrl: NavController, 
              private modalCtrl: ModalController,
              private restaurantProvider: RestaurantServiceProvider) {}

  ngOnInit(){
    this.restaurantProvider.getRestaurants()
        .subscribe((restaurants: Restaurant[])=>{
          this.restaurants = restaurants;
        });
  }

  changeDeliveryDetails(){
    // TODO: Navidate to delivery details page
    console.log('Navigate to Delivery Details page');
  }
  
  openSortingModal(){
    let sortingModal = this.modalCtrl.create(FilterRestaurantModalPage);
    sortingModal.present();
  }

  openRestaurant(id: number){
    this.navCtrl.push(RestaurantPage, {
      restaurantId: id
    })
  }
}
